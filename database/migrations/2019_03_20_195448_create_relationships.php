<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
            });
        Schema::table('people', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
                $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade');
                $table->foreign('program_id')->references('id')->on('programs')->onUpdate('cascade');
            });

        Schema::table('programs', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
            });

        Schema::table('schedules', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
                $table->foreign('novelty_id')->references('id')->on('novelties')->onUpdate('cascade');
            });

        Schema::table('states', function ($table)
            {
                $table->foreign('typestate_id')->references('id')->on('typestates')->onUpdate('cascade');
            });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
