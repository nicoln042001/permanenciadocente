<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    //
    protected $table 'people';
    protected $fillable ['namePerson','state_id','role_id','program_id'];
    protected $guarded ['document'];
}
