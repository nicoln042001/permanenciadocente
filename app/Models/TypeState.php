<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeState extends Model
{
   protected $table = 'typestates';
    protected $fillable = ['typestate'];
    protected $guarded = ['id'];

}
